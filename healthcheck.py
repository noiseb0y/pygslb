#!/usr/bin/env python

"""
Python script for active-passive global load balancing of origins for
a single site via CloudFlare DNS.

Bhavin Tailor, May 2016
"""

import requests
import validators
from configparser import SafeConfigParser
from time import sleep

# Parse config and ASSIGN ALL THE THINGS!

parser = SafeConfigParser()
parser.read('lb.conf')

p_url = parser.get('primary_node', 'healthcheck_url')
s_url = parser.get('secondary_node', 'healthcheck_url')

p_ip = str(parser.get('primary_node', 'ip'))
s_ip = str(parser.get('secondary_node', 'ip'))

s_wait = int(parser.get('healthcheck', 'success_wait'))  
f_wait = int(parser.get('healthcheck', 'fail_wait'))  

s_count = int(parser.get('healthcheck', 'success_count'))
f_count = int(parser.get('healthcheck', 'fail_count'))

api_key = str(parser.get('api', 'key'))
api_email = str(parser.get('api', 'email'))

zone_id = str(parser.get('zone', 'id'))

record_name = str(parser.get('record', 'name'))
record_id = str(parser.get('record', 'id'))

# Function to carry out healthchecks.

def getHealth(p_node):
    fail = 0
    while fail < f_count:
        r = requests.get(p_node)
        if r.status_code is not 200:
            print ("Response code %s, healthcheck fail :(" % r.status_code)
            fail = fail + 1
            sleep(f_wait)
        else:
            print ("Response code 200, healtcheck pass! :D")
            fail = 0
            sleep(s_wait)
        print ("Testing again")
    print ("Site dead, failing over.")

# Update CloudFlare via API. If failure, try 3 times before waiting 30s to try again.
def updateDNS(p_node):
    endpoint = "https://api.cloudflare.com/client/v4/zones/"+zone_id+"/dns_records/"+record_id
    record = {
        "id":record_id,
        "type":"A",
        "name":record_name,
        "content":p_ip
    }
    auth = {
        "X-Auth-Email":api_email,
        "X-Auth-Key":api_key,
    }
    attempts = 0
    print ("Calling API...")
    u = requests.put(endpoint, headers=auth, json=record)
    if u.status_code is not 200:
        while attempts < 3 and u.status_code is not 200:
            sleep(5)    # sleep for 5s before trying again
            print ("API call failed, response code %s, trying again" % u.status_code)
            u = requests.put(endpoint, headers=auth, json=record)
            attempts = attempts + 1
        else:
            print ("Too many failures calling the API. Waiting 30s before trying again.")
            sleep(30)
            updateDNS(p_node)
    else:
        print ("API call succesful, record updated to %s" % p_ip )

# Validate URLs, then carry out healthcheck until failure. If fail, swap primary 
# and secondary, and update DNS.
if __name__ == '__main__':
    try:
        validators.url(p_url)
        validators.url(s_url)
        dead = 0
        while dead is 0:
            while dead is not 1:
                getHealth(p_url)
                dead = 1
            else:
                p_url, s_url = s_url, p_url
                p_ip, s_ip = s_ip, p_ip
                updateDNS(p_url)
                print ("Primary node now %s" % p_url)
                dead = 0
        dead = 0
    except KeyboardInterrupt:
        print ("Keyboard interrupt. KILLING.")
    except:
        print ("Not a valid URL :(")

# The End.
